<?php

require_once '../src/DB/db.php';

class Campaign extends DB{

    function getAll(){
        $query = $this->connect()->query("SELECT * FROM campaign");
        return $query;
    }
    function createCampaign($title,$content,$image){
        $query = $this->connect()->prepare("INSERT INTO campaign(title,content,image) VALUES(:title,:content,:image)");
        $query->execute(["title"=>$title,"content"=>$content,"image"=>$image]);
        return $query;
    }
    function updateCampaign($id,$title,$content,$image){
        if(strlen($image)==0){
            $query = $this->connect()->prepare("UPDATE campaign SET title=:title, content=:content WHERE id=:id");
            $query->execute(["title"=>$title,"content"=>$content,"id"=>$id]);
        }
        else{
            $query = $this->connect()->prepare("UPDATE campaign SET title=:title, content=:content, image=:image WHERE id=:id");
            $query->execute(["title"=>$title,"content"=>$content,"image"=>$image,"id"=>$id]);
        }
        return $query;
    }
    function getById($id){
        $query = $this->connect()->prepare("SELECT * FROM campaign WHERE id=:id");
        $query->execute(["id"=>$id]);
        return $query;
    }
}

?>