<?php

require_once '../src/DB/db.php';

class Promotion extends DB{

    function getAll(){
        $query = $this->connect()->query("SELECT * FROM promotion");
        return $query;
    }
    function createPromotion($title,$content,$image){
        $query = $this->connect()->prepare("INSERT INTO promotion(title,content,image) VALUES(:title,:content,:image)");
        $query->execute(["title"=>$title,"content"=>$content,"image"=>$image]);
        return $query;
    }
    function updatePromotion($id,$title,$content,$image){
        if(strlen($image)==0){
            $query = $this->connect()->prepare("UPDATE promotion SET title=:title, content=:content WHERE id=:id");
            $query->execute(["title"=>$title,"content"=>$content,"id"=>$id]);
        }
        else{
            $query = $this->connect()->prepare("UPDATE promotion SET title=:title, content=:content, image=:image WHERE id=:id");
            $query->execute(["title"=>$title,"content"=>$content,"image"=>$image,"id"=>$id]);
        }
        return $query;
    }
    function getById($id){
        $query = $this->connect()->prepare("SELECT * FROM promotion WHERE id=:id");
        $query->execute(["id"=>$id]);
        return $query;
    }

}

?>