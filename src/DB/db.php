<?php

class DB{

    private $host;
    private $db;
    private $user;
    private $password;
    private $charset;

    public function __construct(){
        $this->host = "localhost";
        $this->db = "id9045741_jsfdb";
        $this->user = "id9045741_yogsothoth";
        $this->password = "D4rkZorrow21";
        $this->charset = "utf8";
    }

    function connect(){

        try{
            
            $connection = "mysql:host=".$this->host.";dbname=".$this->db.";charset=".$this->charset;
            $pdo = new PDO($connection,$this->user,$this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $pdo;

        }catch(PDOException $ex){
            print_r('Error al conectar a la DB : ' . $ex->getMessage());
        }

    }

}

?>