<?php

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\UploadedFile;

require '../vendor/autoload.php';
require '../src/Entities/promotion.php';
require '../src/Entities/campaign.php';
require '../src/Entities/message.php';

$app = new \Slim\App;

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Content-Type', 'application/json');
});

$container = $app->getContainer();
$container['upload_directory'] = "../images";

$app->get("/", function($request, $response){
    return "Welcome !!";
});
/*--------------------------------------------------------------------------*/
/*-PROMOTION----------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
$app->get("/promotion", function($request, $response){
    $promotion = new Promotion();
    $message = new Message();
    $res = $promotion->getAll();
    if($res->rowCount()){
        $obj = $res->fetchAll(PDO::FETCH_OBJ);
        for($i=0;$i<4;$i++){
            $obj[$i]->image="https://policlinicovilla.000webhostapp.com/homepageAPI/images/".$obj[$i]->image;
        }
        return json_encode($obj);
    }
    else{
        $message->makeInfo("No records in this table");
    }
});
$app->post("/promotion/add", function($request, $response){
    $promotion = new Promotion();
    $message = new Message();
    /**/
    $directory = $this->get('upload_directory');
    /**/
    $key = $request->getParam("key");
    $title = $request->getParam("title");
    $content = $request->getParam("content");
    $image = '';
    /**/
    $uploadedFiles = $request->getUploadedFiles();
    $file = $uploadedFiles["file"];
    if ($file->getError() === UPLOAD_ERR_OK) {
        $tempKey = "promotion".$key;
        if(uploadFile($directory,$file,$tempKey)){
            $image = getImageName($tempKey,$file);
            if($promotion->createPromotion($title,$content,$image)){
                $message->makeInfo("Uploaded Succesfully");
            }
            else{
                $message->makeError("Error at upload file");
            }
        }
        else{
            $message->makeError("Error at upload file");
        }
    }
    else{
        $message->makeError("Error at charge File");
    }
    
});
$app->post("/promotion/update", function($request, $response){
    $promotion = new Promotion();
    $message = new Message();
    /**/
    $directory = $this->get('upload_directory');
    /**/
    $key = $request->getParam("key");
    $title = $request->getParam("title");
    $content = $request->getParam("content");
    $imageName = '';
    /**/
    $uploadedFiles = $request->getUploadedFiles();
    $file = $uploadedFiles["file"];
    if ($file->getError() === UPLOAD_ERR_OK) {
        /* Eliminar antigua Imagen */
        $res = $promotion->getById($key);
        if($res->rowCount()){
            $obj = $res->fetchAll(PDO::FETCH_OBJ);
            $deletePath = "../images/".$obj[0]->image;
            unlink($deletePath);
        }
        /*-------------------------*/
        $tempKey = "promotion".$key;
        if(uploadFile($directory,$file,$tempKey)){
            $imageName = getImageName($tempKey,$file);
        }
        else{
            $message->makeError("Error at upload file");
        }
    }
    if($promotion->updatePromotion($key,$title,$content,$imageName)){
        return $response->withRedirect('https://policlinicovilla.000webhostapp.com/homepageAPI/interface/', 301);
        //$message->makeInfo("Updated Succesfully");
    }
    else{
        $message->makeError("Error at updated file");
    }
});
$app->get("/promotion/{id}", function($request, $response){
    $promotion = new Promotion();
    $message = new Message();
    /**/
    $id = $request->getAttribute("id");
    /**/
    $res = $promotion->getById($id);
    if($res->rowCount()){
        $obj = $res->fetchAll(PDO::FETCH_OBJ);
        for($i=0;$i<1;$i++){
            $obj[$i]->image="https://policlinicovilla.000webhostapp.com/homepageAPI/images/".$obj[$i]->image;
        }
        return json_encode($obj);
    }
    else{
        $message->makeInfo("No record in this table");
    }
});

/*--------------------------------------------------------------------------*/
/*-CAMPAIGN-----------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
$app->get("/campaign", function($request, $response){
    $campaign = new Campaign();
    $message = new Message();
    $res = $campaign->getAll();
    if($res->rowCount()){
        $obj = $res->fetchAll(PDO::FETCH_OBJ);
        for($i=0;$i<4;$i++){
            $obj[$i]->image="https://policlinicovilla.000webhostapp.com/homepageAPI/images/".$obj[$i]->image;
        }
        return json_encode($obj);
    }
    else{
        $message->makeInfo("No records in this table");
    }
});
$app->post("/campaign/add", function($request, $response){
    $campaign = new Campaign();
    $message = new Message();
    /**/
    $directory = $this->get('upload_directory');
    /**/
    $key = $request->getParam("key");
    $title = $request->getParam("title");
    $content = $request->getParam("content");
    $image = '';
    /**/
    $uploadedFiles = $request->getUploadedFiles();
    $file = $uploadedFiles["file"];
    if ($file->getError() === UPLOAD_ERR_OK) {
        $tempKey = "campaign".$key;
        if(uploadFile($directory,$file,$tempKey)){
            $image = getImageName($tempKey,$file);
            if($campaign->createCampaign($title,$content,$image)){
                $message->makeInfo("Uploaded Succesfully");
            }
            else{
                $message->makeError("Error at upload file");
            }
        }
        else{
            $message->makeError("Error at upload file");
        }
    }
    else{
        $message->makeError("Error at charge File");
    }
    
});
$app->post("/campaign/update", function($request, $response){
    $campaign = new Campaign();
    $message = new Message();
    /**/
    $directory = $this->get('upload_directory');
    /**/
    $key = $request->getParam("key");
    $title = $request->getParam("title");
    $content = $request->getParam("content");
    $imageName = '';
    /**/
    $uploadedFiles = $request->getUploadedFiles();
    $file = $uploadedFiles["file"];
    if ($file->getError() === UPLOAD_ERR_OK) {
        /* Eliminar antigua Imagen */
        $res = $campaign->getById($key);
        if($res->rowCount()){
            $obj = $res->fetchAll(PDO::FETCH_OBJ);
            $deletePath = "../images/".$obj[0]->image;
            unlink($deletePath);
        }
        /*-------------------------*/
        $tempKey = "campaign".$key;
        if(uploadFile($directory,$file,$tempKey)){
            $imageName = getImageName($tempKey,$file);
        }
        else{
            $message->makeError("Error at upload file");
        }
    }
    if($campaign->updateCampaign($key,$title,$content,$imageName)){
        return $response->withRedirect('https://policlinicovilla.000webhostapp.com/homepageAPI/interface/', 301);
        //$message->makeInfo("Updated Succesfully");
    }
    else{
        $message->makeError("Error at updated file");
    }
    
});
$app->get("/campaign/{id}", function($request, $response){
    $campaign = new Campaign();
    $message = new Message();
    /**/
    $id = $request->getAttribute("id");
    /**/
    $res = $campaign->getById($id);
    if($res->rowCount()){
        $obj = $res->fetchAll(PDO::FETCH_OBJ);
        for($i=0;$i<1;$i++){
            $obj[$i]->image="https://policlinicovilla.000webhostapp.com/homepageAPI/images/".$obj[$i]->image;
        }
        return json_encode($obj);
    }
    else{
        $message->makeInfo("No record in this table");
    }
});

$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});

/*--------------------------------------------------------------------------*/
/*-FUNCTIONS----------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
function uploadFile($directory,$image,$key){
    try{
        $extension = pathinfo($image->getClientFilename(), PATHINFO_EXTENSION);
        $basename = $key;
        $filename = sprintf('%s.%0.8s', $basename, $extension);
    
        $image->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        return true;
    }catch(Exception $ex){
        return false;
    }
}
function getImageName($key,$image){
    $extension = pathinfo($image->getClientFilename(), PATHINFO_EXTENSION);
    $basename = $key;
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    return $filename;
}

?>