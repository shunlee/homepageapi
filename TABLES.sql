-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 06, 2019 at 01:56 PM
-- Server version: 10.3.14-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id9045741_jsfdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE `campaign` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(445) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `title`, `content`, `image`) VALUES
(1, 'Que es una Campaña?', 'Conjunto de actividades o de trabajos que se realizan en un período de tiempo determinado y están encaminados a conseguir un fin.', 'campaign1.jpg'),
(2, 'Que es una Campaña?', 'Conjunto de actividades o de trabajos que se realizan en un período de tiempo determinado y están encaminados a conseguir un fin.', 'campaign2.jpg'),
(3, 'Que es una Campaña?', 'Conjunto de actividades o de trabajos que se realizan en un período de tiempo determinado y están encaminados a conseguir un fin.', 'campaign3.jpg'),
(4, 'Que es una Campaña?', 'Conjunto de actividades o de trabajos que se realizan en un período de tiempo determinado y están encaminados a conseguir un fin.', 'campaign4.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(445) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `title`, `content`, `image`) VALUES
(1, 'Que es una Promoción?', 'La promoción es un término que se utiliza con frecuencia en marketing es uno de los elementos de la mezcla de mercado.', 'promotion1.jpg'),
(2, 'Que es una Promoción?', 'La promoción es un término que se utiliza con frecuencia en marketing es uno de los elementos de la mezcla de mercado.', 'promotion2.jpg'),
(3, 'Que es una Promoción?', 'La promoción es un término que se utiliza con frecuencia en marketing es uno de los elementos de la mezcla de mercado.', 'promotion3.jpg'),
(4, 'Que es una Promoción?', 'La promoción es un término que se utiliza con frecuencia en marketing es uno de los elementos de la mezcla de mercado.', 'promotion4.jpg');

-- --------------------------------------------------------

--
-- Indexes for table `campaign`
--
ALTER TABLE `campaign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaign`
--
ALTER TABLE `campaign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;