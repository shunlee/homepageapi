function changeSelectPromotionValue(){
    const id = document.querySelector("#selectPromotion").value;
    if(id!=0){
        document.querySelector("#titlePromotion").removeAttribute('disabled')
        document.querySelector("#contentPromotion").removeAttribute('disabled')
        document.querySelector("#btnUpdatePromotion").removeAttribute('disabled')
        chargePromo(id);
    }
    else{
        document.querySelector("#titlePromotion").setAttribute('disabled','')
        document.querySelector("#contentPromotion").setAttribute('disabled','')
        document.querySelector("#btnUpdatePromotion").setAttribute('disabled','')
    }
}
function chargePromo(id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const res = JSON.parse(this.responseText);
            document.querySelector("#titlePromotion").value = res[0].title;
            document.querySelector("#contentPromotion").value = res[0].content;
        }
    };
    xhttp.open("GET", "https://policlinicovilla.000webhostapp.com/homepageAPI/public/promotion/"+id, true);
    xhttp.send();
}
/*---------------------------------------------------------------------------------------*/
function changeSelectCampaignValue(){
    const id = document.querySelector("#selectCampaign").value;
    if(id!=0){
        document.querySelector("#titleCampaign").removeAttribute('disabled')
        document.querySelector("#contentCampaign").removeAttribute('disabled')
        document.querySelector("#btnUpdateCampaign").removeAttribute('disabled')
        chargeCampaign(id);
    }
    else{
        document.querySelector("#titleCampaign").setAttribute('disabled','')
        document.querySelector("#contentCampaign").setAttribute('disabled','')
        document.querySelector("#btnUpdateCampaign").setAttribute('disabled','')
    }
}
function chargeCampaign(id) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const res = JSON.parse(this.responseText);
            document.querySelector("#titleCampaign").value = res[0].title;
            document.querySelector("#contentCampaign").value = res[0].content;
        }
    };
    xhttp.open("GET", "https://policlinicovilla.000webhostapp.com/homepageAPI/public/campaign/"+id, true);
    xhttp.send();
}